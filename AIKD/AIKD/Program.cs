﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SevenZip;
namespace AIKD
{
    class Program
    {
        public static void Main(string[] args)
        {
            string zipPath = "C:\\Program Files\\7-Zip\\7z.dll";
            SevenZipBase.SetLibraryPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "7z64.dll"));
            var filePaths = new List<string>
            {
                "C:\\Users\\pczachor\\Documents\\Visual Studio 2015\\Projects\\AIKD\\AIKD\\Pliki testowe\\audio mp3\\audio.mp3"

            };
            foreach (var filePath in filePaths)
            {

                foreach (CompressionLevel compressionLevel in Enum.GetValues(typeof(CompressionLevel)))
                {
                    //Compress(filePath, OutArchiveFormat.SevenZip, compressionLevel, CompressionMethod.Copy,
                    //        CompressionMode.Create);
                    foreach (CompressionMethod compressionMethod in Enum.GetValues(typeof(CompressionMethod)))
                    {
                        Compress(filePath, OutArchiveFormat.SevenZip, compressionLevel, compressionMethod,
                            CompressionMode.Create);
                    }
                }
            }

            Console.ReadKey();
        }

        private static void Compress(string filePath, OutArchiveFormat format, CompressionLevel compressionLevel, CompressionMethod compressionMethod, CompressionMode compressionMode)
        {
            var fileName = Path.GetFileName(filePath);

            var sevenZipCompressor = new SevenZipCompressor
            {
                ArchiveFormat = format,
                CompressionLevel = compressionLevel,
                CompressionMethod = compressionMethod,
                CompressionMode = compressionMode,
            };
            long elapsedMs;
            switch (format)
            {
                case OutArchiveFormat.SevenZip:
                    {
                        var watch = System.Diagnostics.Stopwatch.StartNew();
                        sevenZipCompressor.CompressFiles($"C:\\Users\\pczachor\\Documents\\Visual Studio 2015\\Projects\\AIKD\\AIKD\\Pliki testowe\\{fileName}-{compressionLevel}--{compressionMethod}.7z", filePath);
                        watch.Stop();
                        elapsedMs = watch.ElapsedMilliseconds;
                    }
                    break;

                case OutArchiveFormat.Zip:
                    {
                        var watch = System.Diagnostics.Stopwatch.StartNew();
                        sevenZipCompressor.CompressFiles($"{sevenZipCompressor.TempFolderPath}/{fileName}-{compressionLevel}-{compressionMethod}-{compressionMode}.7z", filePath);
                        watch.Stop();
                        elapsedMs = watch.ElapsedMilliseconds;
                    }
                    break;
            }

            //Console.WriteLine(
            //    $"Czas kompresji pliku {fileName} : {elapsedMs} ms \nRozmiar pliku nieskrompresowanego : {file.Length}\nRozmiar pliku skompresowanego : {compressBytes.Length}\nStopień kompresji : {compresionRate}");
        }
    }
}
